#!/usr/bin/env python3
import re
from subprocess import call
import time

from bs4 import BeautifulSoup
import click
import requests


"""
kürze einen string auf lmax zeichen oder fülle ihn mit entsprechend vielen
spaces auf
"""
def ellipsis(s, lmax):
    if len(s) < lmax:
        return s + (lmax - len(s)) * ' '
    return s[:lmax - 1] + '…'


@click.command()
@click.option('--nitter-url', default='nitter.nl')
@click.argument('tweet')
def main(nitter_url, tweet):
    # nitter hat den schöneren DOM
    tweet = tweet.replace('twitter.com', nitter_url)
    r = requests.get(tweet)
    doc = r.text
    soup = BeautifulSoup(doc, 'html.parser')
    main_thread = soup.find(class_='main-thread')
    tweets = main_thread.find_all(class_='tweet-content')

    # erstmal diesen 1/n blödsinn entfernen
    regex = re.compile(r'\d+/(\d+|n|N|x|X|\*|×)')
    tweets = [re.sub(regex, '', tweet.get_text()) for tweet in tweets]
    tweets[0] = 'ähm naja ich weiß nicht wie ich anfangen soll ähm ' + tweets[0]

    # username extrahieren
    user = '  ' + ellipsis(main_thread.find(class_='fullname').get_text(), 20) + '  '
    click.secho(click.style(f'← 😉 {user} ', fg='white', bg='green'))
    click.secho('')

    # eine nachricht mit drei tränenlachenden smileys darf nicht fehlen
    click.secho(click.style(user, fg='blue', bg='white', bold=True), nl=False)
    click.secho(click.style('◤', fg='white', bg=None))
    click.secho(click.style(' 😂😂😂' + ' ' * 17, fg='black', bg='white'))
    click.secho('')
    
    for tweet in tweets:
        # handle anzeigen
        click.secho(click.style(user, fg='blue', bg='white', bold=True), nl=False)
        click.secho(click.style('◤', fg='white', bg=None))
        
        # play button anzeigen
        click.secho(click.style(' ▶ ' + ' ' * 21, fg='black', bg='white'), nl=False)
        time.sleep(1)
        click.secho(click.style('\r || [' + '-' * 17 + '] ', fg='black', bg='white'), nl=False)
        
        # sprana-feeling erzeugen
        tweet = 'ähm ja ähm ' + tweet
        tweet = tweet.replace('...', '.')
        tweet = tweet.replace('.', ' öööhm... ')
        tweet = tweet.replace(',', ' ääähm... ')

        # espeak beste leben
        call([
            'espeak',
            '-vde+m2',
            tweet
        ])
        click.secho(click.style('\r ▶ ' + ' ' * 21, fg='black', bg='white'), nl=False)
        click.secho('\n')

if __name__ == '__main__':
    main()
